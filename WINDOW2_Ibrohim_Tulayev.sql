

WITH SubcategorySales AS (
    SELECT
        prod_subcategory,
        OrderYear,
        SUM(sales_amount) AS total_sales,
        LAG(SUM(sales_amount), 1) OVER (PARTITION BY prod_subcategory ORDER BY OrderYear) AS prev_year_sales
    FROM
        Sales
    WHERE
        OrderYear BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory, OrderYear
)

SELECT DISTINCT
    prod_subcategory
FROM
    SubcategorySales
WHERE
    total_sales > COALESCE(prev_year_sales, 0);
